import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import reportWebVitals from "./reportWebVitals";
import App from "./App";
import MapPractice from "./ComponentPractice/MapPractice";
import LearnUseState from "./ComponentPractice/LearnUseState/LearnUseState";
import LearnUseState2 from "./ComponentPractice/LearnUseState/LearnUseState2";
import LearnUseState3 from "./ComponentPractice/LearnUseState/LearnUseState3";
import ReactHW from "./ComponentPractice/ReactHW";
import LearnUseState4 from "./ComponentPractice/LearnUseState/LearnUseState4";
import LearnUseStateRendering1 from "./ComponentPractice/LearnUseStateRendering1";
import HideAndShowToggle from "./ComponentPractice/HideAndShowToggle";
import PreventInfiniteLoop from "./ComponentPractice/PreventInfiniteLoop";
import AsyncBehaviourOfUseState from "./ComponentPractice/AsyncBehaviourOfUseState";
import AsyncBehaviourOfUseState2 from "./ComponentPractice/AsyncBehaviourOfUseState2";
import GrandParent from "./ComponentPractice/Propsdrilling/GrandParent";
import LearnUseEffect1 from "./ComponentPractice/LearnUseEffect/LearnUseEffect1";
import LearnUseEffect2 from "./ComponentPractice/LearnUseEffect/LearnUseEffect2";
import { BrowserRouter } from "react-router-dom";
import AppApp from "./AppApp";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  //  <React.StrictMode>
  <div>
    {/* <App 
    name = "nitan"
    address = "gagalphedi"
    age = {29}></App> */}
    {/* <App1></App1> */}
    {/* <MapPractice></MapPractice> */}
    {/* <ReactHW></ReactHW> */}
    {/* <LearnUseState4></LearnUseState> */}
    {/* <LearnUseState4></LearnUseState2> */}
    {/* <LearnUseState3></LearnUseState3> */}
    {/* <LearnUseState4></LearnUseState4> */}
    {/* <LearnUseState4></LearnUseState5> */}
    {/* <LearnUseStateRendering1></LearnUseStateRendering1> */}
    {/* <HideAndShowToggle></HideAndShowToggle> */}
    {/* <PreventInfiniteLoop></PreventInfiniteLoop> */}
    {/* <AsyncBehaviourOfUseState></AsyncBehaviourOfUseState> */}
    {/* <AsyncBehaviourOfUseState2></AsyncBehaviourOfUseState2> */}
    {/* <GrandParent></GrandParent> */}
    {/* <LearnUseEffect1></LearnUseEffect1> */}
    {/* <LearnUseEffect2></LearnUseEffect2> */}
    <BrowserRouter>
      {/* <App></App> */}
      <AppApp></AppApp>
    </BrowserRouter>
  </div>
  // </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

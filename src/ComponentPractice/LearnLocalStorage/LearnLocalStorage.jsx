// import React from "react";

// const LearnLocalStorage = () => {
//   let name1 = "sujan";
//   let age1 = 27;
//   let isMarried1 = false;

//   localStorage.setItem("name", name1);
//   localStorage.setItem("age", age1);
//   localStorage.setItem("isMarried", isMarried1);

// //   In case of arrays and objects:
// //   Since local storage converts all data types to strings:
// //   We need to JSON.stringify non primitive data types, if we want their structural integrity intact.

//   let ar1 = [1, 2, 3];
//   let obj1 = { address: "samakhusi" };

//   localStorage.setItem("array", JSON.stringify(ar1))
//   localStorage.setItem("object", JSON.stringify(obj1))

// //   To get items from browser local storage:

//   console.log(localStorage.getItem("name"))
//   console.log(localStorage.getItem("age"))
//   console.log(localStorage.getItem("isMarried"))

//   console.log(JSON.parse(localStorage.getItem("array")));
//   console.log(JSON.parse(localStorage.getItem(("object"))));


//   return <div></div>;
// };

// export default LearnLocalStorage;

import React, { useRef, useState } from "react";

const LearnUseRef = () => {
  let [name, setName] = useState();

  let nameRef = useRef();
  let ageRef = useRef();
  let sebRef = useRef();

  return (
    <div>
      <div 
      ref={sebRef}
      >I am seb. I am beautiful.</div>

      <label htmlFor="name">Name</label>
      <input
        type="text"
        id="name"
        value={name}
        onChange={(e) => {
          setName(e.target.value);
        }}
        ref={nameRef}
      ></input>

      <br></br>
      <br></br>
      <input ref={ageRef}></input>

      <br></br>
      <br></br>

      <button
        onClick={() => {
          nameRef.current.focus();
        }}
      >
        Focus Input 1
      </button>

      <button
        onClick={() => {
          ageRef.current.focus();
        }}
      >
        Focus Input 2
      </button>

      <button
        onClick={() => {
          sebRef.current.style.backgroundColor = "red";
        }}
      >
        Change background of seb.
      </button>
    </div>
  );
};

export default LearnUseRef;

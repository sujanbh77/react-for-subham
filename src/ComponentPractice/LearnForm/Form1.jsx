import React from 'react'

const Form1 = () => {
    // Text, Number, Password, Email, File, Date, Radio, Checkbox
  return (
    <div>

        <form
        onSubmit={(e)=>{
            e.preventDefault()
            console.log(("Form submitted successfully."))
        }}>
            <label htmlFor="name">Name: </label>
            <input
             type="text"
             id="name"
             onChange={(e)=>{
                console.log(e.target.value)
             }}
             ></input>
            <br></br>

            <label htmlFor="age">Age: </label>
            <input type="number" id="age"></input>
            <br></br>

            <label htmlFor="password">Password: </label>
            <input type="password" id="password"></input>
            <br></br>
            
            <label htmlFor="email">email: </label>
            <input type="email" id="email"></input>
            <br></br>
            
            <label htmlFor="dob">DOB: </label>
            <input type="date" id="dob"></input>
            <br></br>
            
            <label htmlFor="file">Profile: </label>
            <input type="file" id="file"></input>
            <br></br>

            {/* <button type = "button">Send</button> */}
            
            <button type = "submit">Submit</button>
            <input type = "radio"></input>
            <input type = "checkbox"></input>

        </form>
      
    </div>
  )
}

export default Form1

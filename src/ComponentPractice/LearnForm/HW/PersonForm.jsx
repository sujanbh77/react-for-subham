import React, { useState } from 'react'

const PersonForm = () => {
    let [name, setName] = useState("")
    let [age, setAge] = useState("")
    let [roll, setRoll] = useState("")
    let [country, setCountry] = useState("")
  return (
    <div>
      <form
      onSubmit = {(e)=>{
        e.preventDefault()
        console.log("Form submitted successfully.")
      }}
      >
        <div>
            <div>
            <label htmlFor="name">Name: </label>
            <input
            type="text"
            id="name"
            value={name}
            placeholder="Name"
            onChange = {(e)=>{
                setName(e.target.value)
            }}
            >
            </input>
            </div>
            <br></br>

            <div>
            <label htmlFor="age">Age: </label>
            <input
            type="number"
            id="age"
            value={age}
            placeholder="Age"
            onChange = {(e)=>{
                setAge(e.target.value)
            }}
            >
            </input>
            </div>
            <br></br>

            <div>
            <label htmlFor="roll">Roll: </label>
            <input
            type="number"
            id="roll"
            value={roll}
            placeholder="Roll"
            onChange = {(e)=>{
                setRoll(e.target.value)
            }}
            >
            </input>
            </div>
            <br></br>

            <div>
            <label htmlFor="country">Country: </label>
            <input
            type="region"
            id="country"
            value={country}
            placeholder="Country"
            onChange = {(e)=>{
                setCountry(e.target.value)
            }}
            >
            </input>
            </div>
            <br></br>
            
            <div>
                <input type="checkbox"></input>
                I am submitting this form.
            </div>
            <br></br>
            <button type="submit">Submit</button>

        </div>
      </form>
    </div>
  )
}

export default PersonForm

import React, { useState } from 'react'

const ProductForm = () => {
    let [productName, setProductName] = useState("")
    let [productPrice, setProductPrice] = useState("")
    let [productDescription, setProductDescription] = useState("")
  return (
    <div>
        <form
        onSubmit = {(e)=>{
            e.preventDefault()
            console.log("Form submitted successfully.")
        }}
        >
            
            <div>
                <label htmlFor="productName">Product Name: </label>
                <input
                type="text"
                id="productName"
                value={productName}
                placeholder="Product Name"
                onChange = {(e)=>{
                    setProductName(e.target.value)
                }}
                ></input>
            </div>
            <br></br>

            <div>
                <label htmlFor="productPrice">Product Price: </label>
                <input
                type="number"
                id="productPrice"
                value={productPrice}
                placeholder="Product Price"
                onChange = {(e)=>{
                    setProductPrice(e.target.value)
                }}
                ></input>
            </div>
            <br></br>

            <div>
                <label htmlFor="productDescription">Product Description: </label>
                <input
                type="text"
                id="productDescription"
                value={productDescription}
                placeholder="Product Description"
                onChange = {(e)=>{
                    setProductDescription(e.target.value)
                }}
                ></input>
            </div>
            <br></br>

        <div>
                <input type="checkbox"></input>
                I am submitting this form.
            </div>
            <br></br>
            <button type="submit">Submit</button>
      
        </form>
    </div>
  )
}

export default ProductForm

import React, { useState } from "react";

const SelectHW = () => {
  let [name, setName] = useState("");
  let [country, setCountry] = useState("");
  let [gender, setGender] = useState("");

  let countryOptions = [
    { label: "Choose Country", value: "" },
    { label: "Nepal", value: "nepal" },
    { label: "Japan", value: "japan" },
    { label: "Korea", value: "korea" },
  ];

  let genderOptions = [
    { label: "Choose Gender", value: "" },
    { label: "Male", value: "male" },
    { label: "Female", value: "female" },
  ];

  return (
    <div>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          let info = {
            name: name,
            country: country,
            gender: gender,
          };
          console.log(info.name);
          console.log(info.country);
          console.log(info.gender);
        }}
      >
        <div>
          <label htmlFor="name">Name: </label>
          <input
            type="text"
            id="name"
            value={name}
            placeholder="Name"
            onChange={(e) => {
              setName(e.target.value);
            }}
          ></input>
        </div>
        <br></br>

        <div>
          <select
            value={country}
            onChange={(e) => {
              setCountry(e.target.value);
            }}
          >
            {countryOptions.map((item, i) => {
              return (
                <option key={i} value={item.value} disabled={item.disabled}>
                  {item.label}
                </option>
              );
            })}
          </select>
        </div>
        <br></br>

        <div>
          <select
            value={gender}
            onChange={(e) => {
              setGender(e.target.value);
            }}
          >
            {genderOptions.map((item, i) => {
              return (
                <option key={i} value={item.value} disabled={item.disabled}>
                  {item.label}
                </option>
              );
            })}
          </select>
        </div>
        <br></br>

        <div>
          <input type="checkbox"></input>I am submitting this form.
        </div>
        <br></br>
        <button type="submit">Submit</button>
      </form>
    </div>
  );
};

export default SelectHW;

import React, { useState } from "react";

const Form6 = () => {
  let [isMarried, setIsMarried] = useState();

  return (
    <div>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          let data = {
            isMarried: isMarried,
          };
          console.log(data);
        }}
      >
        <label htmlFor="isMarried">IsMarried</label>
        <input
          type="checkbox"
          id="isMarried"
          checked={isMarried === true}
          onChange={(e) => {
            setIsMarried(e.target.checked);
          }}
        ></input>

        <button type="submit">Submit</button>
      </form>
    </div>
  );
};

export default Form6;

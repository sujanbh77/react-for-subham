import React, { useState } from 'react'
import LearnUseState5 from './LearnUseState5'

const LearnUseState4 = () => {
    let [showComp, setShowComp] = useState(true)
  return (
    <div>

      {showComp ? <LearnUseState5></LearnUseState5>: null}
      <button
      onClick={()=> {
        setShowComp(true)
      }}
      >Show Component</button>
      <br></br>
      <button
      onClick={()=> {
        setShowComp(false)
      }}
      >Hide Component</button>

    </div>
  )
}

export default LearnUseState4

import React, { useState } from 'react'

const LearnUseState = () => {
    // let name = "nitan"

    // Defining variable in react using useState hook.
    let [name, setName] = useState("nitan")
    let [surName, setSurName] = useState("thapa")

  return (
    <div>
      name is {name}
      <br></br>
      <button
      onClick = {() => {
        setName("Sujan")
      }}>
      Change name
      </button>
      <br></br>
      surName is {surName}
      <br></br>
      <button
      onClick = {() => {
        setSurName("Bhandari")
      }}>
      Change surname
      </button>

    </div>
  )
}

export default LearnUseState

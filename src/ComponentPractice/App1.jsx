// import React from 'react'

// const App1 = () => {
//     let name = "nitan"
//     let age = 29
//     let isMarried = true          // Boolean are not shown in Browser.(Normal Panel)
//     let list = ["nitan", "ram"]   // Large brackets are also not shown in Browser.
                                     // It is shown with elements concatenated as one. Similar to .join("")
                                     // >> nitanram

//     let obj = {
//         address: "gagalphedi",
//     }                             // Objects are not valid as a react child. !!! DO NOT USE OBJECTS IN <div></div> TAGS !!!
//   return (
//     <div>
//       Name is {name} <br></br>
//       Age is {age}   <br></br>
//       Is he married? {isMarried? "Yes" : "No"}
//     </div>
//   )
// }

// export default App1

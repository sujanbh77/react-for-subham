import React from 'react'
import Child from './Child'

const Parent = (props) => {
  let {name} = props
  return (
    <div>
      I am the Parent.
      <Child name = {name}></Child>
    </div>
  )
}

export default Parent

import React, { useEffect, useState } from 'react'

const LearnUseEffect1 = () => {
    let [count, setCount] = useState(0)
    let [count1, setCount1] = useState(100)
    useEffect(()=>{
        console.log("I am useEffect")
    },[count, count1])
  return (
    <div>
      LearnUseEffect1
      <br></br>
      <button
      onClick = {()=>{
        setCount(count+1)
      }}
      >Click</button>
      <br></br>
       <button
      onClick = {()=>{
        setCount1(count1+1)
      }}
      >Click</button>
    </div>
  )
}

export default LearnUseEffect1

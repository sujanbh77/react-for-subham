// import React, { useEffect, useState } from 'react'

// const LearnCleanUpFunction = () => {
//     let [count, setCount] = useState(0)
//     useEffect(()=>{
//         console.log("a")
//         console.log("b")
//      return ()=> {
//         console.log("I am return.")  
//     }
   
//     }, [count])
//   return (
//     <div>
//       <button
//       onClick = {()=> {
//         setCount(count+1)
//       }}
//       >Click</button>
//     </div>
//   )
// }

// // CleanUp Function:
// // It doesn't execute in first render.

// // In useEffect, return function(CleanUp function) always executes first, followed by the useEffect function.

// // CleanUp function has a special power:
// // The return function executes if the component is removed/deleted.(hidden/shown)


// export default LearnCleanUpFunction

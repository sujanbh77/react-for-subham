import React, { useEffect } from "react";
const LearnCleanUpFunction2 = () => {
  useEffect(() => {
    let timer = setInterval(() => {
      console.log("i will run for every 1s");
    }, 1000);
    return () => {
      clearInterval(timer);
    };
  }, []);
  return <div>LearnCleanUpFunction2</div>;
};
export default LearnCleanUpFunction2;
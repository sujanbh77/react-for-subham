import React from "react";

const LearnOrNullish = () => {

//   OR:

//   let a = 0;
//   let b = false;
//   let c = a || b || null;
//   console.log(c)

// Nullish:

// let a;
// let b = false
// let c = a ?? b ?? null

// console.log(c)

  return <div></div>;
};

export default LearnOrNullish;
